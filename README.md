# torrent-search

## Usage
```
$ torrent-search heartstopper s01e01
{
  title: 'Heartstopper.S01E01-08.DLMux.1080p.E-AC3-AC3.ITA.ENG.SUBS',
  time: "Apr. 22nd '22",
  seeds: 10,
  peers: 18,
  size: '7.5 GB',
  desc: 'https://www.1337x.to/torrent/522..',
  provider: '1337x',
  magnet: 'magnet:?xt=urn:btih:76BD65902A...'
}
```

The magnet link can be copied out manually, or extracted using [jq](https://stedolan.github.io/jq/):
```
$ torrent-search heartstopper s01e01 | jq .magnet -r
```

## Install
Install [NodeJS](https://nodejs.org/en/), and then run
```
$ git clone https://codeberg.org/kira/torrent-search
$ npm install --global ./torrent-search
```

## Disclaimer
This author, of course, does not condone the use of this software for illegal purposes.

