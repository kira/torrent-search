#!/usr/bin/env node

const TorrentSearchApi = require('torrent-search-api')

const searchString = process.argv.slice(2).join(' ')

TorrentSearchApi.enableProvider('1337x')
TorrentSearchApi.enableProvider('Yts')
TorrentSearchApi.enableProvider('KickassTorrents')

TorrentSearchApi.search(searchString)
  .then(torrents => {
    TorrentSearchApi.getMagnet(torrents[0])
      .then(magnet => {
        const torrent = {
          ...torrents[0],
          magnet
        }
        console.log(JSON.stringify(torrent, null, 2))
      }, err => {
        throw err
      })
  },
  err => { throw err })

